<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incidents extends Model
{
    protected $fillable = [
    	'user_id',
    	'user_status',
        'category_id',
    	'police_id',
    	'image',
    	'description',
    	'location',
        'status_id'
    ];

    public function scopeGet_incedents_by_user($user_id)
    {
    	return $this->where('user_id',$user_id);
    }
}
