<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncidentsReports extends Model
{

    protected $fillable = [
    	'incident_id',
    	'police_id',
        'report'
    ];
}
