<?php

namespace App\Abstracts\Transformers;
use Carbon\Carbon;

class IncidentsTransformer extends Transformer {

    public function transform($incidents)
    {
        $image = json_decode($incidents['image']);
        if (empty($image)) {
            $image = '';
        } else {
            $image = url('uploads/thumbs/incidents/'.$image[1]);
        }

        return [
            'id' => $incidents['id'],
            'user_name' => $incidents['user_status'],
            'category' => $incidents['category_id'],
            'description' => $incidents['description'],
            'location' => $incidents['location'],
            'created_at' => $this->convertDateTime($incidents['created_at']),
        ];
    }

    public function convertDateTime($date)
    {
        Carbon::setLocale('id');
        $date = Carbon::createFromTimeStamp(strtotime($date));
        $date = $date->format('l, d F Y');
        return $date;
    }
}