<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

// model
use App\Models\Incidents;
use App\Models\IncidentsReports;
use App\Models\Categories;
use App\Models\UserStatus;

// transformer
// use App\Abstracts;
// use App\Abstracts\Transfromers\IncidentsTransformer;

class IncidentsController extends Controller
{
    /**
     * @var Abstracts\Transformers\IncidentsTransformer
     */
    // protected $IncidentsTransformer;

    // function __construct(IncidentsTransformer $IncidentsTransformer)
    // {
    //     $this->incidentsTransformer = $IncidentsTransformer;
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidents = Incidents::all();
        if ($incidents->isEmpty()) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        for ($i=0;$i<count($incidents);$i++) {
            $incidents[$i]['location'] = json_decode($incidents[$i]['location']);
        }
        return $incidents;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'user_status' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'location' => 'required',
            'image' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()]);
        }
        $user_status = UserStatus::find($request->user_status);
        if (empty($user_status)) {
            return response()->json(['error' => 
                [ "user_status" => 'User status is not found.' ]
            ]);
        }

        $category = Categories::find($request->category_id);
        if (empty($category)) {
            return response()->json(['error' => 
                [ "category_id" => 'Category id is not found.' ]
            ]);
        }

        // upload image
        $image_array = "";
        // $crop_array = array();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_validator = Validator::make($request->all(), [
                'image' => 'mimes:jpeg,jpg,png,bmp,gif|max:500'
            ]);
            if ($image_validator->fails()) {
                return response()->json(['error' => $image_validator->errors()->getMessages()]);
            }
            $type = $image->getClientOriginalExtension();
            $image = time().'.'.$type;
            $image_thumb = time().'_thumb.'.$type;
            $dir = 'uploads/incidents/'.$category->name.'/';
            $dir_thumb = 'uploads/thumbs/incidents/'.$category->name.'/';

            // move upload file to the folder
            $height = ($request->height) ? $request->height : '200';
            $width = ($request->width) ? $request->width : '200';
            $img = Image::make($request->file('image'));
            $img->save($dir.$image);
            $img->crop($width, $height)->save($dir_thumb.$image_thumb);

            // insert in array
            $image_array = url($dir.$image);
            // $image_array['small'] = $dir_thumb.$image_thumb;

            $crop_array['height'] = $request->height;
            $crop_array['width'] = $request->width;
            
            $img->destroy();
        }

        $incidents = new Incidents();
        $incidents->create([
            'user_id' => $request->user_id,
            'user_status' => $request->user_status,
            'category_id' => $category->id,
            'image' => $image_array,
            'description' => $request->description,
            'location' => json_encode($request->location),
            'status_id' => 1
            // 'police_id' => ''
        ]);

        return response()->json(['success' => 'data already sent!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $incidents = Incidents::where('user_id',$user_id)->get();
        if ($incidents->isEmpty()) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        for ($i=0;$i<count($incidents);$i++) {
            $incidents[$i]['location'] = json_decode($incidents[$i]['location']);
        }
        return $incidents;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCategory($category_id)
    {
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $incidents = Incidents::where('category_id',$category_id)->get();
        if ($incidents->isEmpty()) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        for ($i=0;$i<count($incidents);$i++) {
            $incidents[$i]['location'] = json_decode($incidents[$i]['location']);
        }
        return $incidents;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allRequest()
    {
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $incidents = Incidents::where('status_id',1)->get();
        if ($incidents->isEmpty()) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        for ($i=0;$i<count($incidents);$i++) {
            $incidents[$i]['location'] = json_decode($incidents[$i]['location']);
        }
        return $incidents;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request)
    {
        // dd($request->all());
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $validator = Validator::make($request->all(), [
            'incident_id' => 'required',
            'police_id' => 'required',
            'status_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()]);
        }

        $incidents = Incidents::find($request->incident_id);
        if (empty($incidents)) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        // return $incidents;
        $incidents->update([
            'status_id' => $request->status_id,
            'police_id' => $request->police_id
        ]);
        return response()->json(['success' => 'incident has accepted!']);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIncidentsStatusByPolice($status_id,$police_id)
    {
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $incidents = Incidents::where(['police_id' => $police_id])->whereIn('status_id',[2,3])->get();
        if ($incidents->isEmpty()) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        for ($i=0;$i<count($incidents);$i++) {
            $incidents[$i]['location'] = json_decode($incidents[$i]['location']);
        }
        return $incidents;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function incidentReport(Request $request)
    {
        // dd($request->all());
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $validator = Validator::make($request->all(), [
            'incident_id' => 'required',
            'police_id' => 'required',
            'report' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()]);
        }

        $incidents = Incidents::find($request->incident_id);
        if (empty($incidents)) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        $incident_report = new IncidentsReports();
        // return $incidents;
        $incident_report->create([
            'incident_id' => $request->incident_id,
            'police_id' => $request->police_id,
            'report' => $request->report
        ]);
        return response()->json(['success' => 'incident report has accepted!']);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIncidentById($incident_id)
    {
        // $incidents = Incidents::get_incidents_by_user($user_id)->get();
        $incidents = Incidents::find($incident_id);
        if (empty($incidents)) {
            return response()->json(['error' => 
                [ "incidents" => 'Incidents is not found.' ]
            ]);
        }
        $incidents['location'] = json_decode($incidents['location']);
        return $incidents;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
