<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

// user
Route::post('/incident','API\IncidentsController@store');
Route::get('/incident/user/{user_id}','API\IncidentsController@show');
Route::get('/incident/category/{category_id}','API\IncidentsController@showCategory');
Route::get('/incident/timeline','API\IncidentsController@index');
Route::get('/categories','API\CategoriesController@index');

// incidents
Route::get('/incident/{incident_id}','API\IncidentsController@getIncidentById');
// police
Route::get('/incident/request','API\IncidentsController@allRequest');
Route::post('/incident/change_status','API\IncidentsController@changeStatus');
Route::get('/incident/status/{status_id}/police/{police_id}','API\IncidentsController@getIncidentsStatusByPolice');
Route::post('/incident/report','API\IncidentsController@incidentReport');
